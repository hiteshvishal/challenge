import app_pages as app
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys


class Dashboard():
    instance = None

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = Dashboard()
        return cls.instance

    def __init__(self):
        self.driver = app.app.get_driver()

    # navigaton method 
    def navigate_to_form_step_task(self):
        ActionChains(self.driver).move_to_element(self.driver.find_element_by_xpath("//p[text()='Form Step Task']")).perform()

    # form sleection method
    def select_form_step_task(self):
        self.driver.find_element_by_xpath("//p[text()='Form Step Task']").click()

    # screen verification method
    def verify_activity_listpage(self):
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//h1[text()='Tasks']")))
        assert element is not None

    # close browser method
    def close_browser(self):
        self.driver.close()


dashboard = Dashboard.get_instance()

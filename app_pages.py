from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from urllib.parse import urljoin


class App():
    instance = None

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = App()
        return cls.instance

    def __init__(self):
        self.driver = webdriver.Chrome()

    # get driver instance
    def get_driver(self):
        return self.driver

    # url lauch method
    def load_website(self):
        self.driver.maximize_window()
        self.driver.get('https://mystudy.qa.medable.com/?org=pawabqaauto')
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//button[text()='Sign In']")))

    # Application sign in method
    def sign_app(self, usernname, password):
        self.driver.find_element_by_xpath("//button[text()='Sign In']").click()
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//*[@name='email']")))
        self.driver.find_element_by_xpath("//*[@name='email']").send_keys(usernname)
        self.driver.find_element_by_xpath("//*[@name='password']").send_keys(password)
        self.driver.find_element_by_xpath("//button[text()='Log In']").click()
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//button[text()='Ignore']")))
        self.driver.find_element_by_xpath("//button[text()='Ignore']").click()
        WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//h1[text()='Tasks']")))


app = App.get_instance()

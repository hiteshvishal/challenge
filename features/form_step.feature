Feature: Form Step

  Scenario: Form - Patient App Web - Basic
     Given Patient navigate to activity list as "carlos.escribano+challenge2@medable.com"
      And Patient select Form Step Task on activity list page
      And Patient should see Form Step Task task ready to start
      And Patient completed Form step with values
        | Type                  | Value               |
        | Email Step            | a@a.com             |
        | Text Step             | Automation          |
        | Numeric Step          | 21                  |
        | Value Picker Step     | Option 1            |
        | Boolean Step          | True                |
        | Continuous Scale Step | 5.5                 |
        | Integer Step          | 5                   |
        | Location Step         | Córdoba, Cordoba, Argentina  |
        | Text Scale Step       | 1                   |
        | Time Interval Step    | 2 2                 |
        | Time of Day Step      | 1 1 PM              |
      And Patient click next button in step page
      And Patient click skip button in step page
      And Patient should see the completion step pageAnd Patinet click done button in step page
      Then Patient should see the Activity list page
      And Patient should see Form Step Task completed

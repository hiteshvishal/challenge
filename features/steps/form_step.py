import app_pages as app
import tasks_pages as task
import form_step_pages as formstep
from behave import given, when, then




@given(u'Patient navigate to activity list as "carlos.escribano+challenge2@medable.com"')
def step_impl(context):
    app.app.load_website()
    app.app.sign_app('carlos.escribano+challenge2@medable.com','Medable123$')



@given(u'Patient select Form Step Task on activity list page')
def step_impl(context):
    task.dashboard.navigate_to_form_step_task()
    task.dashboard.select_form_step_task()



@given(u'Patient should see Form Step Task task ready to start')
def step_impl(context):
    formstep.formsteptask.verify_form_step_task()


@given(u'Patient completed Form step with values')
def step_impl(context):
    formstep.formsteptask.enter_email_step(context.table[0]['Value'])
    formstep.formsteptask.enter_text_step(context.table[1]['Value'])
    formstep.formsteptask.enter_numeric_step(context.table[2]['Value'])
    formstep.formsteptask.select_value_picker(context.table[3]['Value'])
    formstep.formsteptask.select_boolean_picker(context.table[4]['Value'])
    formstep.formsteptask.select_continous_scale(context.table[5]['Value'])
    formstep.formsteptask.select_integer_scale(context.table[6]['Value'])
    formstep.formsteptask.select_location_step(context.table[7]['Value'])
    formstep.formsteptask.select_text_scale(context.table[8]['Value'])
    formstep.formsteptask.select_time_interval(context.table[9]['Value'].split()[0],context.table[9]['Value'].split()[1])
    formstep.formsteptask.select_time_of_day(context.table[10]['Value'].split()[0],context.table[10]['Value'].split()[1],context.table[10]['Value'].split()[2])

@given(u'Patient click next button in step page')
def step_impl(context):
    formstep.formsteptask.select_next_button()


@given(u'Patient click skip button in step page')
def step_impl(context):
    formstep.formsteptask.select_skip_button()


@given(u'Patient should see the completion step pageAnd Patinet click done button in step page')
def step_impl(context):
    formstep.formsteptask.select_done_button()


@then(u'Patient should see the Activity list page')
def step_impl(context):
    task.dashboard.verify_activity_listpage()


@then(u'Patient should see Form Step Task completed')
def step_impl(context):
    #raise NotImplementedError(u'STEP: Then Patient should see Form Step Task completed')
    task.dashboard.close_browser()
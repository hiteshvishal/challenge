import app_pages as app
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.support.ui import Select


class FormStepTask():
    instance = None

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = FormStepTask()
        return cls.instance

    def __init__(self):
        self.driver = app.app.get_driver()

    # screen verification method
    def verify_form_step_task(self):
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//h1[text()='Form Step Task']")))
        assert element is not None

    # enter email value method
    def enter_email_step(self, email):
        self.driver.find_element_by_xpath("//*[@type='email']").send_keys(email)

    # enter text step method
    def enter_text_step(self, text):
        self.driver.find_element_by_xpath("//*[text()='Text SubStep Form Step 1']//following::input[1]").send_keys(text)

    # enter number value method
    def enter_numeric_step(self, numeric):
        self.driver.find_element_by_xpath("//*[text()='Numeric SubStep Form Step 1']//following::input[1]").send_keys(numeric)

    # select value picker method
    def select_value_picker(self, value_picker):
        self.driver.find_element_by_xpath("//*[text()='Select an answer']").click()
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//li/div[text()='"+value_picker+"']")))
        element.click()

    # select boolean picker method
    def select_boolean_picker(self, bool):
        if bool == "True":
            self.driver.find_element_by_xpath("//div[text()='Yes']").click()
        else:
            self.driver.find_element_by_xpath("//div[text()='No']").click()

    # select continous scale method
    def select_continous_scale(self, scale):
        element1 = self.driver.find_element_by_xpath("//*[text()='Integer Scale SubStep Form Step 1']")
        actions1 = ActionChains(self.driver)
        actions1.move_to_element(element1).perform()
        actions1.reset_actions()
        time.sleep(2)
        element = self.driver.find_element_by_xpath("//*[text()='Continuous Scale SubStep Form Step 1']//following::div[3]/div[3]")
        actions = ActionChains(self.driver)
        actions.click_and_hold(element).perform()
        actions.click(element).perform()
        actions.double_click(element).perform()

        slider = self.driver.find_element_by_xpath("//*[text()='Text Scale SubStep Form Step 1']//following::div[@orientation='horizontal'][3]")
        w, h = slider.size['width'], slider.size['height']
        z = float(w) / float(scale)
        actions = ActionChains(self.driver)
        actions.click_and_hold(element)
        actions.move_by_offset(int((int(element.location['x'])+z-70)), 0).perform()
        actions.reset_actions()
        time.sleep(5)

    # select integet scale method
    def select_integer_scale(self, scale):
        self.driver.find_element_by_xpath("//*[text()='Integer Scale SubStep Form Step 1']//following::div[@tabindex='"+scale+"'][1]").click()

    # select location step method
    def select_location_step(self, location):
        self.driver.find_element_by_xpath("//*[@placeholder='Search']").send_keys(location)
        time.sleep(5)
        self.driver.find_element_by_xpath("//*[@placeholder='Search']").click()
        actions = ActionChains(self.driver)
        actions.send_keys(Keys.ARROW_DOWN).send_keys(Keys.ENTER).perform()
        time.sleep(5)

    # select text scale method
    def select_text_scale(self, scale):
        self.driver.find_element_by_xpath("//*[text()='Text Scale SubStep Form Step 1']//following::div[@tabindex='"+scale+"']").click()

    # select time of interval method
    def select_time_interval(self, hour_val, min_val):
        self.driver.find_element_by_xpath("//button[@name='hour']").click()
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//button[@name='hour']//following::div[@value='"+hour_val+"']")))
        element.click()
        time.sleep(5)
        self.driver.find_element_by_xpath("//button[@name='minute']").click()
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//li/div[@value='"+min_val+"']")))
        element.click()

    # select time of day method
    def select_time_of_day(self, hour, min, date):
        self.driver.find_element_by_xpath("//div[text()='Time of Day SubStep Form Step 1']//following::button[1]").click()
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//div[text()='Time of Day SubStep Form Step 1']//following::div[@value='"+hour+"']")))
        element.click()
        self.driver.find_element_by_xpath("//div[text()='Time of Day SubStep Form Step 1']//following::button[2]").click()
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//div[text()='Time of Day SubStep Form Step 1']//following::div[@value='"+min+"']")))
        element.click()
        self.driver.find_element_by_xpath("//div[text()='Time of Day SubStep Form Step 1']//following::button[3]").click()
        element = WebDriverWait(self.driver, 50).until(EC.presence_of_element_located((By.XPATH, "//div[text()='Time of Day SubStep Form Step 1']//following::div[@value='"+date+"']")))
        element.click()

    # select # next button
    def select_next_button(self):
        element = WebDriverWait(self.driver, 50).until(EC.visibility_of_element_located((By.XPATH, "//button[text()='Next']")))
        element.click()

    # select skip button
    def select_skip_button(self):
        element = WebDriverWait(self.driver, 50).until(EC.visibility_of_element_located((By.XPATH, "//button[text()='Skip']")))
        element.click()

    # Select done button
    def select_done_button(self):
        element = WebDriverWait(self.driver, 50).until(EC.visibility_of_element_located((By.XPATH, "//button[text()='Done']")))
        element.click()


formsteptask = FormStepTask.get_instance()
